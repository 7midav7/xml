package com.marchenko.xmltask.main;

import com.marchenko.xmltask.parser.SaxTariffParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Created by Marchenko Vadim on 10/26/2015.
 */
public class Main {
    private static final Logger LOGGER;
    private static final String LOGS_CONFIG_URI = "config\\log4j2.xml";
    private static final String NAME_PARSING_FILE = "input/tariff.xml";

    static{
        System.setProperty("log4j.configurationFile", LOGS_CONFIG_URI);
        LOGGER = LogManager.getLogger(Main.class);
    }

    public static void main(String[] args) {
        SaxTariffParser saxParser = new SaxTariffParser();
        saxParser.buildArrayTariffs(NAME_PARSING_FILE);
        saxParser.getTariffs().stream().forEach(LOGGER::info);

        SaxTariffParser staxParser = new SaxTariffParser();
        staxParser.buildArrayTariffs(NAME_PARSING_FILE);
        staxParser.getTariffs().stream().forEach(LOGGER::info);

        SaxTariffParser domParser = new SaxTariffParser();
        domParser.buildArrayTariffs(NAME_PARSING_FILE);
        domParser.getTariffs().stream().forEach(LOGGER::info);
    }
}
