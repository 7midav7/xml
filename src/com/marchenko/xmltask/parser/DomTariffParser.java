package com.marchenko.xmltask.parser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created by Marchenko Vadim on 10/19/2015.
 */
public class DomTariffParser extends AbstractTariffBuilder{
    private static final Logger LOGGER = LogManager.getLogger(DomTariffParser.class);

    @Override
    public void buildArrayTariffs(String fileName) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            factory.setFeature("http://apache.org/xml/features/validation/schema", Boolean.TRUE);
            factory.setNamespaceAware(true);
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            Document document = documentBuilder.parse(fileName);
            DocumentConverter converter = new DocumentConverter(document);
            tariffs = converter.convertDocument();
        } catch (ParserConfigurationException e) {
            LOGGER.error("DomTariffParser wasn't configurated", e);
        } catch (SAXException | IOException e) {
            LOGGER.error("DomTariffParser crushed", e);
        }
    }
}
