package com.marchenko.xmltask.parser;

import com.marchenko.xmltask.domain.Tariff;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.util.ArrayDeque;
import java.util.ArrayList;

/**
 * Created by Marchenko Vadim on 10/25/2015.
 */
class StaxContentTariffParser {
    private class Attr {
        public String name;
        public String value;

        public Attr(String name, String value) {
            this.name = name;
            this.value = value;
        }
    }

    private static final Logger LOGGER = LogManager.getLogger();

    private TariffElementsController controller = new TariffElementsController();
    private ArrayList<Tariff> listTariffs = new ArrayList<>();
    private ArrayDeque<TaggedElement> stackCompositeElements = new ArrayDeque<>();

    public ArrayList<Tariff> parse(XMLStreamReader reader) throws XMLStreamException {
        while (reader.hasNext()) {
            int type = reader.next();
            switch (type) {
                case (XMLStreamReader.START_ELEMENT):
                    startElement(reader);
                    break;
                case (XMLStreamReader.END_ELEMENT):
                    endComplexElement(reader);
                    break;
            }
        }
        return listTariffs;
    }

    private void startElement(XMLStreamReader reader) {
        String tag = reader.getLocalName();
        if ("tariffs".equalsIgnoreCase(tag)) {
            return;
        }

        ArrayList<Attr> listAttr = new ArrayList<>();
        for (int i = 0; i < reader.getAttributeCount(); ++i) {
            listAttr.add(new Attr(reader.getAttributeLocalName(i), reader.getAttributeValue(i)));
        }

        if (controller.haveElementsInside(tag)) {
            TaggedElement currentObject = controller.createElement(tag);
            stackCompositeElements.addLast(currentObject);
            fillAttributes(currentObject, listAttr);
        } else {
            try {
                TaggedElement currentObject = controller.createElement(tag, reader.getElementText());
                fillAttributes(currentObject, listAttr);
                TaggedElement parent = stackCompositeElements.peekLast();
                controller.fillField(parent, currentObject);
            } catch (XMLStreamException e) {
                LOGGER.error("Content of tag=" + tag + "wasn't recognized, it replaced by 0");
            }
        }
    }

    private void fillAttributes(TaggedElement currentObject, ArrayList<Attr> listAttr) {
        for (Attr attr : listAttr) {
            TaggedElement attrObject = controller.createElement(attr.name, attr.value);
            controller.fillField(currentObject, attrObject);
        }
    }

    private void endComplexElement(XMLStreamReader reader) {
        String tag = reader.getLocalName();
        if ("tariffs".equalsIgnoreCase(tag)) {
            return;
        }

        TaggedElement currentObject = stackCompositeElements.removeLast();
        if ("tariff".equalsIgnoreCase(tag) || "detailed-tariff".equalsIgnoreCase(tag)) {
            listTariffs.add((Tariff) currentObject.getElement());
        } else {
            TaggedElement parent = stackCompositeElements.peekLast();
            controller.fillField(parent, currentObject);
        }
    }
}
