package com.marchenko.xmltask.parser;

import com.marchenko.xmltask.domain.Tariff;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;


import java.util.ArrayDeque;
import java.util.ArrayList;

/**
 * Created by Marchenko Vadim on 10/20/2015.
 */
class ParsingHandler extends DefaultHandler {
    private ArrayDeque<TaggedElement> stackCompositeElements = new ArrayDeque<>();
    private TariffElementsController controller = new TariffElementsController();
    private ArrayList<Tariff> listTariffs = new ArrayList<>();
    private Attributes lastAttributes;
    private String lastTag;

    @Override
    public void startElement(String s, String name, String s2, Attributes attributes) {
        if ("tariffs".equalsIgnoreCase(name)) {
            return;
        }
        if (controller.haveElementsInside(name)) {
            TaggedElement currentObject = controller.createElement(name);
            stackCompositeElements.addLast(currentObject);
            for (int i = 0; i < attributes.getLength(); ++i) {
                TaggedElement field = controller.createElement(
                        attributes.getLocalName(i), attributes.getValue(i)
                );
                controller.fillField(currentObject, field);
            }
        }
        lastAttributes = attributes;
        lastTag = name;
    }

    @Override
    public void characters(char[] chars, int start, int length) {
        String s = new String(chars, start, length);
        String value = s.trim();
        if (value.matches("\\d+")) {
            TaggedElement parentObject = stackCompositeElements.peekLast();
            TaggedElement currentObject = controller.createElement(lastTag, value);
            for (int i = 0; i < lastAttributes.getLength(); ++i) {
                TaggedElement field = controller.createElement(
                        lastAttributes.getLocalName(i), lastAttributes.getValue(i)
                );
                controller.fillField(currentObject, field);
            }
            controller.fillField(parentObject, currentObject);
        }
    }

    @Override
    public void endElement(String s, String name, String s2) {
        if ("tariffs".equalsIgnoreCase(name)) {
            return;
        }
        if (controller.haveElementsInside(name)) {
            TaggedElement currentTaggedElement = stackCompositeElements.removeLast();
            if ("tariff".equalsIgnoreCase(name) || "detailed-tariff".equalsIgnoreCase(name)) {
                listTariffs.add((Tariff) currentTaggedElement.getElement());
            } else {
                TaggedElement parent = stackCompositeElements.peekLast();
                controller.fillField(parent, currentTaggedElement);
            }
        }
    }

    public ArrayList<Tariff> takeResultsParsing() {
        return listTariffs;
    }
}
