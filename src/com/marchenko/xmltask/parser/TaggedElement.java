package com.marchenko.xmltask.parser;

/**
 * Created by Marchenko Vadim on 10/20/2015.
 */
class TaggedElement {
    private String tag;
    private Object element;

    public TaggedElement(String tag, Object element) {
        this.tag = tag;
        this.element = element;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Object getElement() {
        return element;
    }

    public void setElement(Object element) {
        this.element = element;
    }
}
