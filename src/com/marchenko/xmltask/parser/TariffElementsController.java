package com.marchenko.xmltask.parser;

import com.marchenko.xmltask.domain.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Created by Marchenko Vadim on 10/20/2015.
 */
class TariffElementsController {
    private static final Logger LOGGER = LogManager.getLogger(TariffElementsController.class);

    public TaggedElement createElement(String name) {
        Object temp;
        switch (name.toLowerCase()) {
            case ("bonus"):
                temp = new Bonus();
                break;
            case ("bonus-favorite-number"):
                temp = new BonusFavoriteNumber();
                break;
            case ("tariff"):
                temp = new Tariff();
                break;
            case ("detailed-tariff"):
                temp = new DetailedTariff();
                break;
            case ("call-prices"):
                temp = new CallPricesInCountry();
                break;
            case ("call-prices-out-country"):
                temp = new CallPricesOutCountry();
                break;
            case ("parameters"):
                temp = new Parameters();
                break;
            default:
                LOGGER.warn("Unknown type of element " + name + " in createElement");
                temp = new Object();
        }
        return new TaggedElement(name, temp);
    }

    public TaggedElement createElement(String name, String value) {
        Object temp = new Object();
        switch (name.toLowerCase()) {
            case ("in-network"):
            case ("out-network"):
            case ("stationary"):
            case ("cis"):
            case ("europe"):
            case ("others-countries"):
            case ("video-call-price"):
                temp = new CallPrice(Integer.parseInt(value));
                break;
            case ("payroll"):
            case ("mb-price"):
            case ("sms-price"):
            case ("mms-price"):
            case ("rounding-seconds"):
            case ("count-first-minutes"):
            case ("price-first-minutes"):
            case ("price-tariff"):
            case ("free-mb"):
            case ("free-sms"):
            case ("free-minutes"):
            case ("count-numbers"):
            case ("free-minutes-favorite"):
                temp = Integer.parseInt(value);
                break;
            case ("name"):
            case ("name-operator"):
            case ("website"):
            case ("id"):
                temp = value;
                break;
            default:
                LOGGER.warn("Unknown type of element " + name + " in createElement");
        }
        return new TaggedElement(name, temp);
    }

    public boolean haveElementsInside(String tag) {
        boolean haveElementsInside;
        switch (tag.toLowerCase()) {
            case ("in-network"):
            case ("out-network"):
            case ("stationary"):
            case ("cis"):
            case ("europe"):
            case ("others-countries"):
            case ("video-call-price"):
            case ("payroll"):
            case ("mb-price"):
            case ("sms-price"):
            case ("mms-price"):
            case ("count-first-minutes"):
            case ("price-first-minutes"):
            case ("price-tariff"):
            case ("free-mb"):
            case ("free-sms"):
            case ("free-minutes"):
            case ("count-numbers"):
            case ("free-minutes-favorite"):
            case ("name"):
            case ("name-operator"):
            case ("website"):
            case ("id"):
                haveElementsInside = false;
                break;
            case ("bonus"):
            case ("bonus-favorite-number"):
            case ("tariff"):
            case ("detailed-tariff"):
            case ("call-prices"):
            case ("call-prices-out-country"):
            case ("parameters"):
                haveElementsInside = true;
                break;
            default:
                LOGGER.warn("Unknown type of element in haveElementsInside " + tag);
                haveElementsInside = false;
        }
        return haveElementsInside;
    }

    public boolean fillField(TaggedElement taggedElement, TaggedElement taggedField) {
        boolean isSuccessful = true;
        String name = taggedElement.getTag();
        Object mutableObject = taggedElement.getElement();
        Object field = taggedField.getElement();
        String nameField = taggedField.getTag();
        switch (name.toLowerCase()) {
            case ("bonus"):
                isSuccessful = fillFieldBonus((Bonus) mutableObject, nameField, field);
                break;
            case ("bonus-favorite-number"):
                isSuccessful = fillFieldBonusFavoriteNumber((BonusFavoriteNumber) mutableObject, nameField, field);
                break;
            case ("tariff"):
                isSuccessful = fillFieldTariff((Tariff) mutableObject, nameField, field);
                break;
            case ("detailed-tariff"):
                isSuccessful = fillFieldDetailedTariff((DetailedTariff) mutableObject, nameField, field);
                break;
            case ("call-prices"):
                isSuccessful = fillFieldCallPricesInCountry((CallPricesInCountry) mutableObject, nameField, field);
                break;
            case ("call-prices-out-country"):
                isSuccessful = fillFieldCallPricesOutCountry((CallPricesOutCountry) mutableObject, nameField, field);
                break;
            case ("in-network"):
            case ("out-network"):
            case ("stationary"):
            case ("cis"):
            case ("europe"):
            case ("others-countries"):
            case ("video-call-price"):
                isSuccessful = fillFieldCallPrice((CallPrice) mutableObject, nameField, field);
                break;
            case ("parameters"):
                isSuccessful = fillFieldParameters((Parameters) mutableObject, nameField, field);
                break;
            default:
                LOGGER.warn("Unknown type of element " + name + " in fillField");
                isSuccessful = false;
        }
        return isSuccessful;
    }

    private boolean fillFieldBonus(Bonus bonus, String name, Object obj) {
        boolean isSuccessful = true;
        switch (name.toLowerCase()) {
            case ("free-mb"):
                bonus.setCountFreeMb((Integer) obj);
                break;
            case ("free-sms"):
                bonus.setCountFreeSms((Integer) obj);
                break;
            case ("free-minutes"):
                bonus.setCountFreeMinutes((Integer) obj);
                break;
            default:
                LOGGER.warn("Unknown type of element in fillFieldBonus " + name);
                isSuccessful = false;
        }
        return isSuccessful;
    }

    private boolean fillFieldBonusFavoriteNumber(BonusFavoriteNumber bonus, String name, Object obj) {
        boolean isSuccessful = true;
        switch (name.toLowerCase()) {
            case ("count-numbers"):
                bonus.setCountFavoriteNumbers((Integer) obj);
                break;
            case ("free-minutes-favorite"):
                bonus.setCountFreeMinutesFavorite((Integer) obj);
                break;
            default:
                isSuccessful = fillFieldBonus(bonus, name, obj);
                if (!isSuccessful) {
                    LOGGER.warn("Unknown type of element in fillFieldBonusFavoriteNumber " + name);
                }
        }
        return isSuccessful;
    }

    private boolean fillFieldTariff(Tariff tariff, String name, Object obj) {
        boolean isSuccessful = true;
        switch (name.toLowerCase()) {
            case ("payroll"):
                tariff.setPayroll((Integer) obj);
                break;
            case ("call-prices"):
                tariff.setCallPricesInCountry((CallPricesInCountry) obj);
                break;
            case ("mb-price"):
                tariff.setMbPrice((Integer) obj);
                break;
            case ("sms-price"):
                tariff.setSmsPrice((Integer) obj);
                break;
            case ("parameters"):
                tariff.setParameters((Parameters) obj);
                break;
            case ("name"):
                tariff.setName((String) obj);
                break;
            case ("name-operator"):
                tariff.setNameOperator((String) obj);
                break;
            case ("id"):
                tariff.setId((String) obj);
                break;
            case ("website"):
                tariff.setId((String) obj);
                break;
            default:
                isSuccessful = false;
                LOGGER.warn("Unknown type of element in fillFieldTariff " + name);
        }
        return isSuccessful;
    }

    private boolean fillFieldDetailedTariff(DetailedTariff tariff, String name, Object obj) {
        boolean isSuccessful = true;
        switch (name.toLowerCase()) {
            case ("call-prices-out-country"):
                tariff.setPricesOutCountry((CallPricesOutCountry) obj);
                break;
            case ("video-call-price"):
                tariff.setVideoCallPrice((CallPrice) obj);
                break;
            case ("mms-price"):
                tariff.setMmsPrice((Integer) obj);
                break;
            default:
                isSuccessful = fillFieldTariff(tariff, name, obj);
                if (!isSuccessful) {
                    LOGGER.warn("Unknown type of element in fillFieldDetailedTariff " + name);
                }
        }
        return isSuccessful;
    }

    private boolean fillFieldCallPricesInCountry(CallPricesInCountry prices, String name, Object obj) {
        boolean isSuccessful = true;
        switch (name.toLowerCase()) {
            case ("in-network"):
                prices.setInNetwork((CallPrice) obj);
                break;
            case ("out-network"):
                prices.setOutNetwork((CallPrice) obj);
                break;
            case ("stationary"):
                prices.setStationary((CallPrice) obj);
                break;
            default:
                LOGGER.warn("Unknown type of element in fillFieldCallPricesInCountry " + name);
                isSuccessful = false;
        }
        return isSuccessful;
    }

    private boolean fillFieldCallPricesOutCountry(CallPricesOutCountry prices, String name, Object obj) {
        boolean isSuccessful = true;
        switch (name.toLowerCase()) {
            case ("cis"):
                prices.setCis((CallPrice) obj);
                break;
            case ("europe"):
                prices.setEurope((CallPrice) obj);
                break;
            case ("others-countries"):
                prices.setOthersCountries((CallPrice) obj);
                break;
            default:
                LOGGER.warn("Unknown type of element in fillFieldCallPricesOutCountry " + name);
                isSuccessful = false;
        }
        return isSuccessful;
    }

    private boolean fillFieldCallPrice(CallPrice price, String name, Object obj) {
        boolean isSuccessful = true;
        switch (name.toLowerCase()) {
            case ("rounding-seconds"):
                price.setRoundingSeconds((Integer) obj);
                break;
            case ("count-first-minutes"):
                price.setCountFirstMinutes((Integer) obj);
                break;
            case ("price-first-minutes"):
                price.setPriceFirstMinutes((Integer) obj);
                break;
            default:
                LOGGER.warn("Unknown type of element in fillFieldCallPrice " + name);
                isSuccessful = false;
        }
        return isSuccessful;
    }

    private boolean fillFieldParameters(Parameters parameters, String name, Object obj) {
        boolean isSuccessful = true;
        switch (name.toLowerCase()) {
            case ("price-tariff"):
                parameters.setPriceTariff((Integer) obj);
                break;
            case ("bonus"):
            case ("bonus-favorite-number"):
                parameters.setBonus((Bonus) obj);
                break;
            default:
                LOGGER.warn("Unknown type of element in fillFieldParameters " + name);
                isSuccessful = false;
        }
        return isSuccessful;
    }
}
