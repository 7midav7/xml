package com.marchenko.xmltask.parser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;

/**
 * Created by Marchenko Vadim on 10/19/2015.
 */
public class SaxTariffParser extends AbstractTariffBuilder{
    private static final Logger LOGGER = LogManager.getLogger(SaxTariffParser.class);

    @Override
    public void buildArrayTariffs(String fileName) {
        ParsingHandler handler = new ParsingHandler();
        try{
            XMLReader reader = XMLReaderFactory.createXMLReader();
            reader.setFeature("http://apache.org/xml/features/validation/schema", Boolean.TRUE);
            reader.setContentHandler(handler);
            reader.parse(fileName);
        } catch (SAXException e) {
            LOGGER.warn("Factory didn't create a reader", e);
        } catch (IOException e) {
            LOGGER.warn("File wasn't read", e);
        }
        tariffs = handler.takeResultsParsing();
    }
}
