package com.marchenko.xmltask.parser;

import com.marchenko.xmltask.domain.Tariff;
import org.w3c.dom.*;

import java.util.ArrayList;

/**
 * Created by Marchenko Vadim on 10/24/2015.
 */
class DocumentConverter {
    private Document document;
    private TariffElementsController controller = new TariffElementsController();

    public DocumentConverter(Document document) {
        this.document = document;
    }

    public ArrayList<Tariff> convertDocument(){
        ArrayList<Tariff> listTariffs = new ArrayList<>();
        Element root = document.getDocumentElement();
        NodeList nodeList = root.getChildNodes();
        for (int i=0; i<nodeList.getLength(); ++i){
            TaggedElement child = convertNode(nodeList.item(i));
            if (child != null) {
                listTariffs.add((Tariff) child.getElement());
            }
        }
        return listTariffs;
    }

    private TaggedElement convertNode(Node node){
        String nameNode = node.getLocalName();
        if (nameNode==null){
            return null;
        }

        TaggedElement object;
        String textContent = node.getTextContent();
        if (textContent != null && !controller.haveElementsInside(nameNode)){
            object = controller.createElement(nameNode, textContent);
        } else {
            object = controller.createElement(nameNode);
        }

        NamedNodeMap map = node.getAttributes();
        for (int i=0; i<map.getLength(); ++i){
            Node attribute = map.item(i);
            String nameAttribute = attribute.getLocalName();
            String value = attribute.getNodeValue();
            TaggedElement attributeObject = controller.createElement(nameAttribute, value);
            controller.fillField(object, attributeObject);
        }

        NodeList nodeList = node.getChildNodes();
        for (int i=0; i<nodeList.getLength(); ++i){
            TaggedElement child = convertNode(nodeList.item(i));
            if (child != null) {
                controller.fillField(object, child);
            }
        }
        return object;
    }
}
