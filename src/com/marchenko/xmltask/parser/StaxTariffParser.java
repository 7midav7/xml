package com.marchenko.xmltask.parser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.*;

/**
 * Created by Marchenko Vadim on 10/19/2015.
 */
public class StaxTariffParser extends AbstractTariffBuilder{
    private static final Logger LOGGER = LogManager.getLogger(AbstractTariffBuilder.class);

    @Override
    public void buildArrayTariffs(String fileName) {
        try(InputStream input = new FileInputStream(new File(fileName))) {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            XMLStreamReader reader = inputFactory.createXMLStreamReader(input);
            StaxContentTariffParser handler = new StaxContentTariffParser();
            tariffs = handler.parse(reader);
        } catch (FileNotFoundException e) {
            LOGGER.error("File not found",e);
        } catch (IOException | XMLStreamException e) {
            LOGGER.error("Parser was crushed", e);
        }
    }
}
