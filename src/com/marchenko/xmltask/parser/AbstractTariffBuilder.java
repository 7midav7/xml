package com.marchenko.xmltask.parser;

import com.marchenko.xmltask.domain.Tariff;

import java.util.ArrayList;

/**
 * Created by Marchenko Vadim on 10/26/2015.
 */
public abstract class AbstractTariffBuilder {
    protected ArrayList<Tariff> tariffs;

    public AbstractTariffBuilder(){
        tariffs = new ArrayList<>();
    }

    public ArrayList<Tariff> getTariffs(){
        return tariffs;
    }

    public abstract void buildArrayTariffs(String fileName);
}
