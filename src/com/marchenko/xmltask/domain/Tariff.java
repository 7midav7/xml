package com.marchenko.xmltask.domain;

/**
 * Created by Marchenko Vadim on 10/19/2015.
 */
public class Tariff {
    private int payroll;
    private int mbPrice;
    private int smsPrice;
    private CallPricesInCountry callPricesInCountry;
    private Parameters parameters;
    private String name;
    private String nameOperator;
    private String id;
    private String website;

    public int getPayroll() {
        return payroll;
    }

    public void setPayroll(int payroll) {
        this.payroll = payroll;
    }

    public CallPricesInCountry getCallPricesInCountry() {
        return callPricesInCountry;
    }

    public void setCallPricesInCountry(CallPricesInCountry callPricesInCountry) {
        this.callPricesInCountry = callPricesInCountry;
    }

    public int getMbPrice() {
        return mbPrice;
    }

    public void setMbPrice(int mbPrice) {
        this.mbPrice = mbPrice;
    }

    public int getSmsPrice() {
        return smsPrice;
    }

    public void setSmsPrice(int smsPrice) {
        this.smsPrice = smsPrice;
    }

    public Parameters getParameters() {
        return parameters;
    }

    public void setParameters(Parameters parameters) {
        this.parameters = parameters;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameOperator() {
        return nameOperator;
    }

    public void setNameOperator(String nameOperator) {
        this.nameOperator = nameOperator;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Override
    public String toString() {
        return "Tariff:" +
                "\npayroll=" + payroll +
                "\ncallPricesInCountry=" + callPricesInCountry +
                "\nmbPrice=" + mbPrice +
                "\nsmsPrice=" + smsPrice +
                "\nparameters=" + parameters +
                "\nname=" + name +
                "\nnameOperator=" + nameOperator +
                "\nid=" + id +
                "\nwebsite='" + website;
    }
}
