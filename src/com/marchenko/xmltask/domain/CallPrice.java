package com.marchenko.xmltask.domain;

/**
 * Created by Marchenko Vadim on 10/19/2015.
 */
public class CallPrice {
    private int price;
    private int roundingSeconds;
    private int countFirstMinutes;
    private int priceFirstMinutes;

    public CallPrice(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getRoundingSeconds() {
        return roundingSeconds;
    }

    public void setRoundingSeconds(int roundingSeconds) {
        this.roundingSeconds = roundingSeconds;
    }

    public int getCountFirstMinutes() {
        return countFirstMinutes;
    }

    public void setCountFirstMinutes(int countFirstMinutes) {
        this.countFirstMinutes = countFirstMinutes;
    }

    public int getPriceFirstMinutes() {
        return priceFirstMinutes;
    }

    public void setPriceFirstMinutes(int priceFirstMinutes) {
        this.priceFirstMinutes = priceFirstMinutes;
    }

    @Override
    public String toString() {
        return "CallPrice:" +
                "\nprice=" + price +
                "\nroundingSeconds=" + roundingSeconds +
                "\ncountFirstMinutes=" + countFirstMinutes +
                "\npriceFirstMinutes=" + priceFirstMinutes;
    }
}
