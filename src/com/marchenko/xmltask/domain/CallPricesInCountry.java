package com.marchenko.xmltask.domain;

/**
 * Created by Marchenko Vadim on 10/19/2015.
 */
public class CallPricesInCountry {
    private CallPrice inNetwork;
    private CallPrice outNetwork;
    private CallPrice stationary;

    public CallPrice getInNetwork() {
        return inNetwork;
    }

    public void setInNetwork(CallPrice inNetwork) {
        this.inNetwork = inNetwork;
    }

    public CallPrice getOutNetwork() {
        return outNetwork;
    }

    public void setOutNetwork(CallPrice outNetwork) {
        this.outNetwork = outNetwork;
    }

    public CallPrice getStationary() {
        return stationary;
    }

    public void setStationary(CallPrice stationary) {
        this.stationary = stationary;
    }

    @Override
    public String toString() {
        return "CallPricesInCountry:" +
                "\ninNetwork=" + inNetwork +
                "\noutNetwork=" + outNetwork +
                "\nstationary=" + stationary;
    }
}
