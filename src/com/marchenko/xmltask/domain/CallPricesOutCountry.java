package com.marchenko.xmltask.domain;

/**
 * Created by Marchenko Vadim on 10/19/2015.
 */
public class CallPricesOutCountry {
    private CallPrice cis;
    private CallPrice europe;
    private CallPrice othersCountries;

    public CallPrice getCis() {
        return cis;
    }

    public void setCis(CallPrice cis) {
        this.cis = cis;
    }

    public CallPrice getEurope() {
        return europe;
    }

    public void setEurope(CallPrice europe) {
        this.europe = europe;
    }

    public CallPrice getOthersCountries() {
        return othersCountries;
    }

    public void setOthersCountries(CallPrice othersCountries) {
        this.othersCountries = othersCountries;
    }

    @Override
    public String toString() {
        return "CallPricesOutCountry:" +
                "\ncis=" + cis +
                "\neurope=" + europe +
                "\nothersCountries=" + othersCountries;
    }
}
