package com.marchenko.xmltask.domain;

/**
 * Created by Marchenko Vadim on 10/19/2015.
 */
public class Bonus {
    private int countFreeMb;
    private int countFreeSms;
    private int countFreeMinutes;

    public int getCountFreeMb() {
        return countFreeMb;
    }

    public void setCountFreeMb(int countFreeMb) {
        this.countFreeMb = countFreeMb;
    }

    public int getCountFreeSms() {
        return countFreeSms;
    }

    public void setCountFreeSms(int countFreeSms) {
        this.countFreeSms = countFreeSms;
    }

    public int getCountFreeMinutes() {
        return countFreeMinutes;
    }

    public void setCountFreeMinutes(int countFreeMinutes) {
        this.countFreeMinutes = countFreeMinutes;
    }

    @Override
    public String toString() {
        return "Bonus:" +
                "\ncountFreeMb=" + countFreeMb +
                "\ncountFreeSms=" + countFreeSms +
                "\ncountFreeMinutes=" + countFreeMinutes;
    }
}
