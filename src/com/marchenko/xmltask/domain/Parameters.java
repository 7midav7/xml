package com.marchenko.xmltask.domain;

/**
 * Created by Marchenko Vadim on 10/19/2015.
 */
public class Parameters {
    private int priceTariff;
    private Bonus bonus;

    public int getPriceTariff() {
        return priceTariff;
    }

    public void setPriceTariff(int priceTariff) {
        this.priceTariff = priceTariff;
    }

    public Bonus getBonus() {
        return bonus;
    }

    public void setBonus(Bonus bonus) {
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return "Parameters:" +
                "\npriceTariff=" + priceTariff +
                "\nbonus=" + bonus;
    }
}
