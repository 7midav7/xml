package com.marchenko.xmltask.domain;

/**
 * Created by Marchenko Vadim on 10/19/2015.
 */
public class DetailedTariff extends Tariff {
    private int mmsPrice;
    private CallPrice videoCallPrice;
    private CallPricesOutCountry pricesOutCountry;

    public CallPrice getVideoCallPrice() {
        return videoCallPrice;
    }

    public void setVideoCallPrice(CallPrice videoCallPrice) {
        this.videoCallPrice = videoCallPrice;
    }

    public int getMmsPrice() {
        return mmsPrice;
    }

    public void setMmsPrice(int mmsPrice) {
        this.mmsPrice = mmsPrice;
    }

    public CallPricesOutCountry getPricesOutCountry() {
        return pricesOutCountry;
    }

    public void setPricesOutCountry(CallPricesOutCountry pricesOutCountry) {
        this.pricesOutCountry = pricesOutCountry;
    }

    @Override
    public String toString() {
        return "DetailedTariff:\n" + super.toString() +
                "\nvideoCallPrice=" + videoCallPrice +
                "\nmmsPrice=" + mmsPrice +
                "\npricesOutCountry=" + pricesOutCountry;
    }
}
