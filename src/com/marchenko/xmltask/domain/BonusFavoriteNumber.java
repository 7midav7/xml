package com.marchenko.xmltask.domain;

/**
 * Created by Marchenko Vadim on 10/19/2015.
 */
public class BonusFavoriteNumber extends Bonus {
    private int countFavoriteNumbers;
    private int countFreeMinutesFavorite;

    public int getCountFavoriteNumbers() {
        return countFavoriteNumbers;
    }

    public void setCountFavoriteNumbers(int countFavoriteNumbers) {
        this.countFavoriteNumbers = countFavoriteNumbers;
    }

    public int getCountFreeMinutesFavorite() {
        return countFreeMinutesFavorite;
    }

    public void setCountFreeMinutesFavorite(int countFreeMinutesFavorite) {
        this.countFreeMinutesFavorite = countFreeMinutesFavorite;
    }

    @Override
    public String toString() {
        return "BonusFavoriteNumber:\n" + super.toString() +
                "\ncountFavoriteNumbers=" + countFavoriteNumbers +
                "\ncountFreeMinutesFavorite=" + countFreeMinutesFavorite;
    }
}
